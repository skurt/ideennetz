package de.skurt

class Idea {
	
	String content
	Date dateCreated
	//Set neighbours = new HashSet()
	
	static hasMany = [neighbours: Idea]

    static constraints = {
		//neighbours nullable:true
    }
}
